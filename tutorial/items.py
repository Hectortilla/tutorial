# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst, MapCompose


def remove_splash(value):
    return value.replace('/', '_')

class TedEuropaItem(scrapy.Item):
    # define the fields for your item here like:
    numero_de_referencia = scrapy.Field(
        input_processor=MapCompose(remove_splash),
        output_processor=TakeFirst()
    )
    
    titulo = scrapy.Field(
        output_processor=TakeFirst()
    )
    
    descripcion = scrapy.Field(
        output_processor=TakeFirst()
    )

