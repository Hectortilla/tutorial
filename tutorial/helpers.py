xml_encoding = '<?xml version="1.0" encoding="utf-8"?>'

def json2xml(json_obj, line_padding="", root=None):
    res = _json2xml(json_obj, line_padding="")
    if root:
        return xml_encoding + '\n' + "<{}>\n".format(root) + res + "\n</{}>".format(root)
    return xml_encoding + '\n' + res

def _json2xml(json_obj, line_padding=""):
    result_list = list()

    json_obj_type = type(json_obj)

    if json_obj_type is list:
        for sub_elem in json_obj:
            result_list.append(_json2xml(sub_elem, line_padding))

        return "\n".join(result_list)

    if json_obj_type is dict:
        for tag_name in json_obj:
            sub_obj = json_obj[tag_name]
            
            if type(sub_obj) == str and sub_obj.startswith('http'):
                sub_obj = "<![CDATA[" + sub_obj + "]]>"
            
            result_list.append("%s<%s>" % (line_padding, tag_name))
            result_list.append(_json2xml(sub_obj, "\t" + line_padding))
            result_list.append("%s</%s>" % (line_padding, tag_name))

        return "\n".join(result_list)

    return "%s%s" % (line_padding, json_obj)