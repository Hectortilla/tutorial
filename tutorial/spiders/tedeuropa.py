import scrapy

from tutorial.items import TedEuropaItem
from scrapy.loader import ItemLoader


class   TedEuropaSpider(scrapy.Spider):
    name = "tedeuropa"

    def start_requests(self):
        urls = [
            'https://etendering.ted.europa.eu/cft/cft-search.html?text=&caList=&_caList=1&status=&startDateFrom=&startDateTo=&closingDateFrom=&closingDateTo=&_procedureType=1&confirm=Buscar#',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        detail_page_urls = response.xpath('//table[@id="row"]//tbody//tr//td[2]//a/@href').extract()
        next_page_url = response.xpath('//a[text()="Next"]/@href').extract()

        for detail_page_url in detail_page_urls:
            yield scrapy.Request(
                detail_page_url,
                self.parse_detail_page,
            )

        if next_page_url:
            yield scrapy.Request(
                next_page_url[0],
                self.parse,
            )

    def parse_detail_page(self, response):
        detail_page_loader = ItemLoader(item=TedEuropaItem(), response=response)
        detail_page_loader.add_xpath('numero_de_referencia', '//span[@id="cft.data.internal_name"]/text()')
        detail_page_loader.add_xpath('titulo', '//span[@id="cft_titleautolinked"]/text()')
        detail_page_loader.add_xpath('descripcion', '//span[@id="cft_descriptionautolinked"]/text()')

        yield dict(detail_page_loader.load_item())
