# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
from tutorial.helpers import json2xml
from tutorial.config import RESULT_OUTPUT_PATH


class TedEuropaPipeline(object):
    def process_item(self, item, spider):
        xml_string = json2xml(item, root='Concursos')

        file_path = os.path.join(RESULT_OUTPUT_PATH, item['numero_de_referencia'] + '.xml')
        with open(file_path, 'w') as f:
            f.write(xml_string) 

        return item
